SI004
Joao Cardoso 45103
Jose Mendes 45087


- Como executar(Cliente e servidor):

Para executar estes comandos temos de ter as seguintes diretorias na directoria primaria do java Project:
Directorias no Client:
	/out (para ter as class para executar o client)
	/repositorios (local para ter os repositorios do utilizador)

Directorias no Servidor:
	/out (para ter as class para executar o servidor)
	/RepositoriosInServer (para ter todos os repositorios dos utilizadores)
	/utilizadores (pasta que contem os mac e ficheiro dos utilizadoes)

	Nota: na pasta utilizadores irá ter apenas o ficheiro macFile (mac) e users.cif (ficheiro cifrado), mas no inicio do servidor nao irá haver nenhum
	deles pois serão criados apos o primeiro utilizador se registar ao sistema.


- Cliente:

Para executar o cliente temos de compilar primeiro as classes para isso utilizamos o seguinte comando:

javac -d out -sourcepath src src/Client/Client.java

-----------------------------

Para criar um repositorio fazemos o seguinte comando(utilizar na diretoria primaria):

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client -init <Repname>

-----------------------------

Para fazer um push do repositorio:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client <username> <ip>:<porto> -push <Repname>
Exemplos:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -push rep
java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -p badpw -push rep/relatorio.pdf
java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -push maria/rep
java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -p badpw -push maria/rep/relatorio.pdf

-----------------------------

Para fazer um pull do repositorio:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client <username> <ip>:<porto> -pull <Repname>
Exemplos:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -pull rep
java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -p badpw -pull rep/relatorio.pdf
java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -pull maria/rep
java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -p badpw -pull maria/rep/relatorio.pdf

-----------------------------

Para fazer um -share com outro utilizador:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client <username> <ip>:<porto> -share <Repname> <usertoshare>

Exemplos:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client ze 10.101.148.12:23456 -share rep maria

-----------------------------

Para fazer um -remove de um utilizador:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out Client.Client <username> <ip>:<porto> -remove <Repname> <usertoshare>

Exemplos:

java -Djava.security.manager -Djava.security.policy==client.policy -cp out ze 10.101.148.12:23456 -remove rep maria



- Servidor:

javac -d out -sourcepath src src/Server/Server.java

Inciar:
Nota o porto so aceita o porto 23456

java -Djava.security.manager -Djava.security.policy==server.policy -cp out Server.Server <porto> <password>

Exemplo:

java -Djava.security.manager -Djava.security.policy==server.policy -cp out Server.Server 23456 aminhapassword
