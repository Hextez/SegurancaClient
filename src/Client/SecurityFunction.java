package Client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class SecurityFunction {
	
	public static SecretKey createSimetricKey() throws NoSuchAlgorithmException, NoSuchPaddingException{
		KeyGenerator kg = KeyGenerator.getInstance("AES");
	    kg.init(128);
	    SecretKey key = kg.generateKey();
	    
	    return key;
	}
	
	
	public static byte[] Signature(byte[] data) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException, InvalidKeyException, SignatureException{
		
		FileInputStream fileInputStream = new FileInputStream("keystore.file");
	    KeyStore keyStore = KeyStore.getInstance("JKS");
	    keyStore.load(fileInputStream, "trotinete".toCharArray());
	    Key privateKey = keyStore.getKey("dd", "trotinete".toCharArray());

		Signature s = Signature.getInstance("SHA256withRSA");
		s.initSign((PrivateKey) privateKey);
		s.update(data);
		
		return s.sign();
	}
	

	
	
	
	
	
	

}
