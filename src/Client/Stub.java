package Client;

/**
 * S.I Grupo 4
 * Joao Cardoso 45103
 * Jose Mendes 45087
 */


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.xml.bind.DatatypeConverter;


public class Stub {

	private Socket socket;
	private ObjectOutputStream outStream;
	private ObjectInputStream inStream;


	public Stub(String address) throws NumberFormatException, UnknownHostException, IOException{
		String[] s = address.split(":");
		SocketFactory sf = SSLSocketFactory.getDefault( );
		socket =  sf.createSocket(s[0],Integer.parseInt(s[1]) );
		outStream = new ObjectOutputStream(socket.getOutputStream());
		inStream = new ObjectInputStream(socket.getInputStream());
	}

	public Socket getSocket(){
		return socket;
	}

	public void closeSocket() throws IOException{
		outStream.close();
		inStream.close();
		socket.close();
	}

	public String sendUserInfo(String username, String password) throws IOException, ClassNotFoundException, NoSuchAlgorithmException, InvalidKeyException{
		outStream.writeObject(new String (username));
		outStream.flush();
		

		String resp = (String) inStream.readObject();
		if (resp.equals("new")){
			String checkPass = null;
			System.out.println("Confirma a sua password :");
			Scanner in = new Scanner(System.in);
			while ((checkPass = in.next()) == null){
				System.out.println("Insira a sua password:");
				in = new Scanner(System.in);
			}
			if (checkPass.equals(password)){
				outStream.writeBoolean(true);
				outStream.flush();
				outStream.writeObject(new String (password));
				outStream.flush();
			}else{
				outStream.writeBoolean(false);
				outStream.flush();
				
			}
			
			
		}else{
			String nounce = (String) inStream.readObject();
			Mac mac = Mac.getInstance("HmacSHA256");
			byte[] pass = (password+nounce).getBytes();
			SecretKey key = new SecretKeySpec(pass, "HmacSHA256");
			mac.init(key);
			outStream.writeObject(new String(DatatypeConverter.printHexBinary(mac.doFinal())));
			outStream.flush();
		}

		return (String)inStream.readObject();
		
	}

	public String pushFunction(String reqFileName, String username) throws IOException, ClassNotFoundException, UnrecoverableKeyException, InvalidKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		String[] rep = reqFileName.split("/");
		String hold = reqFileName;
		File file = new File("repositorios/"+reqFileName);
		if(file.isFile() && file.exists()){
			String[] filesinfo;
			if (rep.length == 2){
				String[] filesInfo = {"-push", "file", username,rep[0], rep[1],String.valueOf(file.lastModified()) };
				filesinfo = filesInfo;

			}else{
				String[] filesInfo = {"-push", "file", rep[0], rep[1],rep[2],String.valueOf(file.lastModified()) };
				filesinfo = filesInfo;
			}
			outStream.writeObject(filesinfo);
			outStream.flush();
			if (inStream.readBoolean()){
				pushFile(file);
				List<String> results = (List<String>) inStream.readObject();
				String result = "";
				for (String string : results) {
					result+=string;
				}
				return result ;
			}else{
				List<String> results = (List<String>) inStream.readObject();
				String result = "";
				for (String string : results) {
					result+=string;
				}
				return result ;
			}
		}else if(file.isDirectory() && file.exists()){
			String[] filesinfo;
			if (rep.length == 1){
				String[] filesInfo = {"-push", "directory", username,reqFileName};
				filesinfo = filesInfo;

			}else{

				String[] filesInfo = {"-push", "directory", rep[0], rep[1]};
				filesinfo = filesInfo;
			}
			outStream.writeObject(filesinfo);
			outStream.flush();

			File[] fileInDir = file.listFiles();
			outStream.writeInt(fileInDir.length);
			outStream.flush();
			List<String> results = null;
			for (File file2 : fileInDir) {
				String[] coisas = {file2.getName(), String.valueOf(file2.lastModified())};
				outStream.writeObject(coisas);
				outStream.flush();
				if (inStream.readBoolean()){
					pushFile(file2);
				}
			}
			results = (List<String>) inStream.readObject();
			String result = "";
			for (String string : results) {
				result+=string;
			}
			return result;

		}else{
			return "O ficheiro/diretoria nao existe";
		}
	}

	public void pushFile(File filename) throws IOException, UnrecoverableKeyException, InvalidKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		String[] pa = filename.getAbsolutePath().split("\\.");
		String pathCif;
		if (pa.length == 1){
		
		 	pathCif = filename.getAbsolutePath();
		}else {
			pathCif = filename.getAbsolutePath().substring(0,filename.getAbsolutePath().length()-4);
		}
		
		
		Path path = Paths.get(filename.getAbsolutePath());
		byte[] data = Files.readAllBytes(path);
		
		byte[] signature = SecurityFunction.Signature(data);

		//uso do fileInput e do buffer para guardar no array o texto do ficheiro
		

		outStream.writeObject(signature);
		outStream.flush();
		outStream.writeObject(data);
		outStream.flush();
		
		SecretKey key = SecurityFunction.createSimetricKey();
		
		Cipher c = Cipher.getInstance("AES");
	    c.init(Cipher.ENCRYPT_MODE, key);
	    
	    
		outStream.writeObject(key.getEncoded());
		outStream.flush();
		
		File cif = new File(pathCif+".cif");
	    FileInputStream fis = new FileInputStream(filename);
		CipherOutputStream cos = new CipherOutputStream(new FileOutputStream(cif), c);
	    byte[] b = new byte[16];  
	    int i = fis.read(b);
	    while (i != -1) {
	        cos.write(b, 0, i);
	        i = fis.read(b);
	    }
	    cos.close();
	    
	    outStream.writeInt((int)cif.length());
		outStream.flush();
		//uso do fileInput e do buffer para guardar no array o texto do ficheiro
		FileInputStream fileInputStream = new FileInputStream(cif);
		BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

		byte[] array = new byte[16];
		int n;
		while((n=bufferedInputStream.read(array,0,16)) != -1){
			outStream.write(array, 0, n);
			outStream.flush();
		}

		bufferedInputStream.close();
		fileInputStream.close();
	    cif.delete();


	}

	public void pullFile(File filename, Long lastModified) throws IOException, ClassNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, CertificateException, KeyStoreException, SignatureException {
		String[] pa = filename.getAbsolutePath().split("\\.");
		String pathCif;
		if (pa.length == 1){
		
		 	pathCif = filename.getAbsolutePath();
		}else {
			pathCif = filename.getAbsolutePath().substring(0,filename.getAbsolutePath().length()-4);
		}

		Key key = (Key) inStream.readObject();
		
		byte[] chaveCifrada = new byte[256];
		Cipher c1 = Cipher.getInstance("AES");
		c1.init(Cipher.DECRYPT_MODE, key);
		
		FileOutputStream fos = new FileOutputStream(pathCif+".cif"); //ficheiro onde escrever
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		int fileSize = inStream.readInt();
		byte[] buffer = new byte[16]; //um tamanho fixo do que pode esperar
		int n;
		int current = 0;
		while(current < fileSize){
			n = inStream.read(buffer, 0, fileSize-current < 16 ? fileSize-current:16);
			bos.write(buffer, 0 , n);//writing byteArray to file
			bos.flush(); 
			current+=n;
		}
		bos.close();
		fos.close();
		
		byte[] data = (byte[]) inStream.readObject();
		byte[] signature = (byte[]) inStream.readObject();
		
		FileInputStream fileInputStream = new FileInputStream("keystore.file");
	    KeyStore keyStore = KeyStore.getInstance("JKS");
	    keyStore.load(fileInputStream, "trotinete".toCharArray());
	    Certificate certificate = keyStore.getCertificate("dd");
	    
		PublicKey pk = certificate.getPublicKey( );
		
		Signature s = Signature.getInstance("SHA256withRSA");
		s.initVerify(pk);
		s.update(data);
		if (s.verify(signature)){
			FileInputStream cos = new FileInputStream(pathCif+".cif");
		    CipherInputStream asduos = new CipherInputStream(cos, c1);
		    FileOutputStream fileOutputStream = new FileOutputStream(filename);
		    //BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
		    
		    int size = asduos.read(chaveCifrada);
		    while (size != -1) {
		    	fileOutputStream.write(chaveCifrada, 0, size);
		    	fileOutputStream.flush();
		    	size = asduos.read(chaveCifrada);
		    }
		    //bufferedOutputStream.close();
		    asduos.close();
		    fileOutputStream.close();
		    new File(pathCif+".cif").delete();

			filename.setLastModified(lastModified);
		}
		else{
			System.out.println("Message was corrupted");
		}
		

	}

	public String pullFunction(String reqFileName, String username) throws IOException, ClassNotFoundException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, CertificateException, KeyStoreException, SignatureException{
		String[] rep = reqFileName.split("/");

		String[] toSend = {"-pull", reqFileName};
		outStream.writeObject(toSend);
		outStream.flush();
		
		String type = (String) inStream.readObject();
		String result = "";
		if (type == null){
			
		}else if (type.compareTo("file") == 0){
			if (inStream.readBoolean()){
				if(rep.length == 2){
					File repositorio = new File("repositorios/"+rep[0]);
					if(!repositorio.exists() && ! repositorio.isDirectory()){
						repositorio.mkdir();
					}

				}else if (rep.length > 2){
					File repositorio = new File("repositorios/"+rep[0]);
					File repo = new File("repositorios/"+rep[0]+"/"+rep[1]);

					if(!repositorio.exists() && ! repositorio.isDirectory()){
						repositorio.mkdir();
						repo.mkdir();
					}else{
						if(!repo.exists() && !repo.isDirectory()){
							repo.mkdir();
						}
					}

				}
				Long lastModified = inStream.readLong();
				File ficheiro = new File("repositorios/"+reqFileName);
				if (ficheiro.exists() && ficheiro.isFile()){
					if (ficheiro.lastModified() < lastModified){
						outStream.writeBoolean(true);
						outStream.flush();
						pullFile(ficheiro, lastModified);
					}else{
						outStream.writeBoolean(false);
						outStream.flush();
					}
				}else{
					outStream.writeBoolean(true);
					outStream.flush();
					pullFile(ficheiro,lastModified);
				}
			}
		}else if (type.compareTo("directory") == 0){

			if (inStream.readBoolean()){
				List<String> files = (List<String>) inStream.readObject();
				File repos = new File("repositorios");
				for (String filename : files) {
					if (inStream.readBoolean()){
						if(rep.length == 1){
							File repositorio = new File("repositorios/"+rep[0]);
							if(!repositorio.exists() && ! repositorio.isDirectory()){
								repositorio.mkdir();
							}

						}else if (rep.length > 1){
							File repositorio = new File("repositorios/"+rep[0]);
							File repo = new File("repositorios/"+rep[0]+"/"+rep[1]);

							if(!repositorio.exists() && ! repositorio.isDirectory()){
								repositorio.mkdir();
								repo.mkdir();
							}else{
								if(!repo.exists() && !repo.isDirectory()){
									repo.mkdir();
								}
							}

						}
						Long lastModified = inStream.readLong();
						File ficheiro = new File("repositorios/"+reqFileName+"/"+filename);
						if (ficheiro.exists() && ficheiro.isFile()){
							if (ficheiro.lastModified() < lastModified){
								outStream.writeBoolean(true);
								outStream.flush();
								pullFile(ficheiro, lastModified);
							}else if (ficheiro.lastModified() > lastModified){
								System.out.println(filename + "tem de fazer o push deste ficheiro");
								outStream.writeBoolean(false);
								outStream.flush();
							}else{
								outStream.writeBoolean(false);
								outStream.flush();
							}
						}else{
							outStream.writeBoolean(true);
							outStream.flush();
							pullFile(ficheiro,lastModified);
						}
					}
				}
				File repos1 = new File("repositorios/"+reqFileName);
				File[] FilesInRep = repos1.listFiles();
				for (File file : FilesInRep) {
					if (! files.contains(file.getName())){
						System.out.println(file.getName() + " este ficheiro nao se encontra no servidor pedimos que faça um push deles");
					}
				}
			}
		}


		for (String string : (List<String>)inStream.readObject()) {
			result= result + string;
		}
		return result;
	}

	public String addUserToShare(String username, String reqFileName, String userTo) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		String[] info = {"-share", username, reqFileName, userTo};
		outStream.writeObject(info);
		outStream.flush();

		return (String) inStream.readObject();
	}

	public String unshareUser(String username, String reqFileName, String userTo) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		String[] info = {"-remove", username, reqFileName, userTo};
		outStream.writeObject(info);
		outStream.flush();

		return (String) inStream.readObject();
	}

}
