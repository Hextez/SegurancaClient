package Client;

/**
 * S.I Grupo 4
 * Joao Cardoso 45103
 * Jose Mendes 45087
 */

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Client {

	public static void main(String argv[]) throws IOException, ClassNotFoundException, InvalidKeyException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException, CertificateException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		File file;
		System.setProperty("javax.net.ssl.trustStore", "myClient.keyStore");
		String password = null;
		String action = null;
		String reqFileName = null;
		String userTo = null;
		String username = null;

		if (argv.length == 0 || argv.length == 1){
			System.out.println("Precisa dos argumentos");
			return;
		}
		
		if (argv[0].equals("-init") && argv.length == 2){
			String [] info = argv[1].split("/");
			file = new File("repositorios/"+argv[1]);
			if (file.exists() && file.isDirectory()){
				System.out.println("Esse repositorio ja existe");
			}else if (info.length== 1){
				boolean result = false;
				try{
					file.mkdir();
					System.out.println(file.getAbsolutePath());
					result = true;
				} 
				catch(SecurityException se){
					//handle it
				}        
				if(result) {    
					System.out.println("repositorio criado");  
				}
			}else {
				System.out.println("Nao e possivel criar repositorio");
			}
			return;

		}
		Stub connection;
		if (argv.length == 2){//new user com apenas username, address
			username = argv[0];
			System.out.println("Insira a sua password:");
			Scanner in = new Scanner(System.in);
			while ((password = in.next()) == null){
				System.out.println("Insira a sua password:");
				in = new Scanner(System.in);
			}
			connection = new Stub(argv[1]);
			if (! connection.sendUserInfo(username, password).equals("NewUser")){
				connection.closeSocket();
				System.out.println("Utilizador ja existe");
			}else{
				connection.closeSocket();
				System.out.println("Novo utilizador");
			}
			return;
		}else if (argv[2].equals("-p") && argv.length == 4){ //com password apenas o registo
			username = argv[0];
			password = argv[3];
			connection = new Stub(argv[1]);
			try{
				String res = connection.sendUserInfo(username, password);
				if(res.equals("Fail")){
					connection.closeSocket();
					System.out.println("enganou-se");
				}else if (!res.equals("NewUser")){
					connection.closeSocket();
					System.out.println("Utilizador ja existe");
			
				}else{
					connection.closeSocket();
					System.out.println("Novo utilizador");
				}
			}catch (Exception e){
				System.out.println("O sistema teve uma falha");
			}
			return;
		}else if(argv[2].equals("-p") && (argv.length == 6 || argv.length == 7)){// com password e execucao de uma accao
			username = argv[0];
			password = argv[3];
			connection = new Stub(argv[1]);
			if((argv[4].equals("-push") || argv[4].equals("-pull")) && argv.length == 6){//se e push ou pull
				action = argv[4];
				reqFileName = argv[5];
			}else if((argv[4].equals("-share") || argv[4].equals("-remove")) && argv.length == 7 ){//se e share ou remove
				action = argv[4];
				reqFileName = argv[5];
				userTo = argv[6];
			}else{
				System.out.println("Parametros invalidos");
				connection.closeSocket();

				return;
			}
		}else if(! argv[2].equals("-p") && (argv.length == 4 || argv.length == 5)){//quaso nao tem a password
			username = argv[0];
			System.out.println("Insira a sua password:");
			Scanner in = new Scanner(System.in);
			while ((password = in.next()) == null){
				System.out.println("Insira a sua password:");
				in = new Scanner(System.in);
			}
			connection = new Stub(argv[1]);
			if((argv[2].equals("-push") || argv[2].equals("-pull")) && argv.length == 4){//o mesmo que os outros
				action = argv[2];
				reqFileName = argv[3];
			}else if((argv[2].equals("-share") || argv[2].equals("-remove")) && argv.length == 5 ){//o mesmo que os outros
				action = argv[2];
				reqFileName = argv[3];
				userTo = argv[4];
			}else{
				System.out.println("parametros invalidos");
				connection.closeSocket();
				return;
			}
		}else{
			System.out.println("parameteros invalidos");
			return;
		}
		String login = connection.sendUserInfo(username, password);

		if (login.equals("LoginSucc") || login.equals("NewUser")){
			if (action != null){
				String result;
				switch (action) {
				case "-push":
					System.out.println("A funcao push vai ser executada");
					result = connection.pushFunction(reqFileName, username);
					System.out.println(result);
					break;
				case "-pull": 
					System.out.println("A funcao pull vai ser executada");
					result = connection.pullFunction(reqFileName, username);
					System.out.println(result);
					break;
				case "-share":		
					System.out.println("A funcao share vai ser executada");
					result = connection.addUserToShare(username, reqFileName, userTo);
					System.out.println(result);
					break;
				case "-remove":
					System.out.println("A funcao remove vai ser executada");
					result = connection.unshareUser(username, reqFileName, userTo);
					System.out.println(result);
					break;
				default:
					System.out.println("action invalid");
					break;
				}
			}
		}else{
			System.out.println("Login failed");
		}
		connection.closeSocket();

	}

	
}
